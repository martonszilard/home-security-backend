import numpy as np
import tensorflow as tf
import cv2
import os.path
from keras import backend as K
# from keras.models import model_from_json
# from keras.preprocessing import image as image_proc

from webapp.config import Config

class EmotionDetector():
    def __init__(self, name=None):
        self.name = name
        self.__load_model__()

    def __load_model__(self):
        config = Config()

        directory_path = "{}/{}".format(config.MODEL_DIRECTORY_PATH,
                                        config.EMOTION_MODEL_FILE_NAME)
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

            tf.keras.utils.get_file(fname="{}.zip".format(config.EMOTION_MODEL_FILE_NAME),
                                    origin=config.SAVED_MODEL_URL,
                                    cache_dir=config.MODEL_DIRECTORY_PATH,
                                    cache_subdir='',
                                    extract=True)

        tf.compat.v1.enable_eager_execution()
        tf.compat.v1.disable_v2_behavior()

        # Load model from JSON file
        json_file = open(directory_path + 'fer.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = tf.keras.models.model_from_json(loaded_model_json)
        global graph
        global sess
        graph = tf.compat.v1.get_default_graph()
        sess = tf.compat.v1.Session()
        self.emotions = ['neutral', 'happiness', 'surprise', 'sadness', 'anger', 'disgust', 'fear']

        # Load weights and them to model
        self.model.load_weights(directory_path + 'fer.h5')
        K.clear_session()
        tf.compat.v1.reset_default_graph()
        self.classifier = cv2.CascadeClassifier(directory_path + 'haarcascade_frontalface_default.xml')

    def detect(self, image):
        if image is None:
            raise Exception("Detector-detect error: image is None")

        states = []
        probabilites = []

        image_np = np.array(cv2.imdecode(
            np.fromstring(image, np.uint8), cv2.IMREAD_COLOR))
        gray_image = cv2.cvtColor(image_np, cv2.COLOR_BGR2GRAY)
        faces_detected = self.classifier.detectMultiScale(gray_image, 1.18, 5)
        # print(self.model.summary())

        for (x, y, w, h) in faces_detected:

            cv2.rectangle(image_np, (x, y), (x + w, y + h), (0, 255, 0), 2)
            roi_gray = gray_image[y:y + w, x:x + h]
            roi_gray = cv2.resize(roi_gray, (48, 48))
            roi_gray = roi_gray[np.newaxis, ..., np.newaxis]
            print(roi_gray.shape)
            #img_pixels = tf.keras.preprocessing.image.img_to_array(roi_gray)
            #roi_gray = np.expand_dims(roi_gray, axis=0)
            #img_pixels /= 255.0
            with graph.as_default():
                with sess.as_default():
                    predictions = self.model(roi_gray)
                    print(predictions.eval())
            pred_array = None
            
            max_index = int(np.argmax(pred_array))
            predicted_emotion = self.emotions[max_index]
            states.append(predicted_emotion)
            probabilites.append(np.max(pred_array) * 100)

        resized_img = cv2.resize(image_np, (1024, 768))
        cv2.imshow('Facial Emotion Recognition', resized_img)
        return states, probabilites
