import tensorflow as tf

from django.apps import AppConfig
from os import getenv


DEFAULT_MODEL_DIRECTORY_PATH = './detector_models'
DEFAULT_EMOTION_MODEL_FILE_NAME = 'emotion_model/'
DEFAULT_FACE_MODEL_FILE_NAME = 'face_recognition_model/'
DEFAULT_SPEECH_MODEL_FILE_NAME = 'voice_model/'


class Config(AppConfig):
    name = 'ViolenceRec'
    MODEL_DIRECTORY_PATH = getenv('MODEL_DIRECTORY_PATH')
    EMOTION_MODEL_FILE_NAME = getenv('MODEL_FILE_NAME')
    FACE_MODEL_FILE_NAME = getenv('MODEL_FILE_NAME')
    SPEECH_MODEL_FILE_NAME = getenv('MODEL_FILE_NAME')

    def __init__(self, name=None):
        self.name = name
        if self.MODEL_DIRECTORY_PATH is None:
            self.MODEL_DIRECTORY_PATH = DEFAULT_MODEL_DIRECTORY_PATH

        if self.EMOTION_MODEL_FILE_NAME is None:
            self.EMOTION_MODEL_FILE_NAME = DEFAULT_EMOTION_MODEL_FILE_NAME

        if self.FACE_MODEL_FILE_NAME is None:
            self.FACE_MODEL_FILE_NAME = DEFAULT_FACE_MODEL_FILE_NAME

        if self.SPEECH_MODEL_FILE_NAME is None:
            self.SPEECH_MODEL_FILE_NAME = DEFAULT_SPEECH_MODEL_FILE_NAME
