import numpy as np
import cv2
import argparse
import sys
import os

ROOT = os.path.dirname(os.path.abspath(__file__))+"/../"
CURR_PATH = os.path.dirname(os.path.abspath(__file__))+"/"
sys.path.append(ROOT)

import detection.utils.movement_utils.lib_images_io as lib_images_io
import detection.utils.movement_utils.lib_plot as lib_plot
import detection.utils.movement_utils.lib_commons as lib_commons
from detection.utils.movement_utils.lib_openpose import SkeletonDetector
from detection.utils.movement_utils.lib_tracker import Tracker
from detection.utils.movement_utils.lib_classifier import *  # Import all sklearn related libraries
from detection.utils.movement_utils.multi_person_classifier import MultiPersonClassifier

from webapp.config import Config


def par(path):  # Pre-Append ROOT to the path if it's not absolute
    return ROOT + path if (path and path[0] != "/") else path

SRC_MODEL_PATH = ""

# -- Settings

CLASSES = ['stand', 'walk', 'run', 'jump', 'sit', 'squat', 'kick', 'punch', 'wave']
SKELETON_FILENAME_FORMAT = "{:05d}.txt"

# Action recognition: number of frames used to extract features.
WINDOW_SIZE = 5

# Video setttings

# If data_type is video, set the sampling interval.
# For example, if it's 3, then the video will be read 3 times faster.
SRC_VIDEO_SAMPLE_INTERVAL = 1

# Openpose settings
OPENPOSE_MODEL = "mobilenet_thin"
OPENPOSE_IMG_SIZE = "656x368"

# Display settings
img_disp_desired_rows = 480


# -- Function

def remove_skeletons_with_few_joints(skeletons):
    ''' Remove bad skeletons before sending to the tracker '''
    good_skeletons = []
    for skeleton in skeletons:
        px = skeleton[2:2+13*2:2]
        py = skeleton[3:2+13*2:2]
        num_valid_joints = len([x for x in px if x != 0])
        num_leg_joints = len([x for x in px[-6:] if x != 0])
        total_size = max(py) - min(py)
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # IF JOINTS ARE MISSING, TRY CHANGING THESE VALUES:
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if num_valid_joints >= 5 and total_size >= 0.1 and num_leg_joints >= 0:
            # add this skeleton only when all requirements are satisfied
            good_skeletons.append(skeleton)
    return good_skeletons

class MovementDetector():

    def __init__(self, name=None):
        self.name = name
        self.__load_model__()

    def __load_model__(self):
        config = Config()

        directory_path = "{}/{}".format(config.MODEL_DIRECTORY_PATH,
                                        config.EMOTION_MODEL_FILE_NAME)
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

            tf.keras.utils.get_file(fname="{}.zip".format(config.EMOTION_MODEL_FILE_NAME),
                                    origin=config.SAVED_MODEL_URL,
                                    cache_dir=config.MODEL_DIRECTORY_PATH,
                                    cache_subdir='',
                                    extract=True)
        # GPU ran out of memory trying to initialize the model. Set memory growth,
        # so the runtime initialization will not allocate all memory on the device.
        #physical_devices = tf.config.list_physical_devices('GPU')
        #tf.config.experimental.set_memory_growth(physical_devices[0], True)

        # self.skeleton_detector = SkeletonDetector(OPENPOSE_MODEL, OPENPOSE_IMG_SIZE)

        # self.multiperson_tracker = Tracker()

        # self.multiperson_classifier = MultiPersonClassifier(SRC_MODEL_PATH, CLASSES)

    # def detect(self, image):

    #     if image is None:
    #         raise Exception("Detector-detect error: image is None")

    #     # -- Detect skeletons
    #     humans = skeleton_detector.detect(image)
    #     skeletons, scale_h = skeleton_detector.humans_to_skels_list(humans)
    #     skeletons = remove_skeletons_with_few_joints(skeletons)

    #     # -- Track people
    #     dict_id2skeleton = multiperson_tracker.track(
    #         skeletons)  # int id -> np.array() skeleton

    #     # -- Recognize action of each person
    #     if len(dict_id2skeleton):
    #         dict_id2label = multiperson_classifier.classify(
    #             dict_id2skeleton)

    #     # Print label of a person
    #     if len(dict_id2skeleton):
    #         min_id = min(dict_id2skeleton.keys())
    #         print("prediced label is :", dict_id2label[min_id])
    #         print("asd ", dict_id2label)
    #         result = self.detector.predict('detection/test.wav')

    #     return result