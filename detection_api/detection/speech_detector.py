import tensorflow as tf
import pyaudio
import os
import wave
from sys import byteorder
from array import array
from struct import pack
from sklearn.ensemble import GradientBoostingClassifier, BaggingClassifier
from detection.utils.speech_utils.utils import get_best_estimators
from detection.utils.speech_utils.speech_recognition import EmotionRecognizer

from webapp.config import Config

THRESHOLD = 500
CHUNK_SIZE = 1024
FORMAT = pyaudio.paInt16
RATE = 16000

SILENCE = 30

class SpeechDetector():

    def __init__(self, name=None):
        self.name = name
        self.__load_model__()

    def get_estimators_name(self, estimators):
        result = [ '"{}"'.format(estimator.__class__.__name__) for estimator, _, _ in estimators ]
        return ','.join(result), {estimator_name.strip('"'): estimator for estimator_name, (estimator, _, _) in zip(result, estimators)}

    def __load_model__(self):
        config = Config()

        directory_path = "{}/{}".format(config.MODEL_DIRECTORY_PATH,
                                        config.EMOTION_MODEL_FILE_NAME)
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

            tf.keras.utils.get_file(fname="{}.zip".format(config.EMOTION_MODEL_FILE_NAME),
                                    origin=config.SAVED_MODEL_URL,
                                    cache_dir=config.MODEL_DIRECTORY_PATH,
                                    cache_subdir='',
                                    extract=True)
        # GPU ran out of memory trying to initialize the model. Set memory growth,
        # so the runtime initialization will not allocate all memory on the device.
        #physical_devices = tf.config.list_physical_devices('GPU')
        #tf.config.experimental.set_memory_growth(physical_devices[0], True)

        estimators = get_best_estimators(True)
        estimators_str, estimator_dict = self.get_estimators_name(estimators)
        emotions = "neutral,calm,happy,sad,angry,fear,disgust"
        features = ["mfcc", "chroma", "mel"]
        self.detector = EmotionRecognizer(estimator_dict['MLPClassifier'], emotions=emotions.split(","), features=features, verbose=0)
        # self.detector.train()
        # print("Test accuracy score: {:.3f}%".format(self.detector.test_score()*100))

    def detect(self, speech):

        if speech is None:
            raise Exception("Detector-detect error: speech is None")

        with open("detection/audio_file.wav", "wb") as file:
            file.write(speech)

        result = self.detector.predict('detection/audio_file.wav')
        return result
        