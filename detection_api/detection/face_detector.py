import cv2
import numpy as np
import os 

from webapp.config import Config

#iniciate id counter
id = 0 #input('\n enter user id end press <return> ==>  ')

# names related to ids: example ==> Marcelo: id=1,  etc
names = ['None', 'Szilard', 'Reka', 'Balazs'] 

# Define min window size to be recognized as a face
minW = 64.0
minH = 48.0

persons, probabilites = [], []

class FaceDetector():
    def __init__(self, name=None):
        self.name = name
        self.__load_model__()

    def __load_model__(self):
        config = Config()

        directory_path = "{}/{}".format(config.MODEL_DIRECTORY_PATH,
                                        config.FACE_MODEL_FILE_NAME)
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)

            tf.keras.utils.get_file(fname="{}.zip".format(config.FACE_MODEL_FILE_NAME),
                                    origin=config.SAVED_MODEL_URL,
                                    cache_dir=config.MODEL_DIRECTORY_PATH,
                                    cache_subdir='',
                                    extract=True)

        self.recognizer = cv2.face.LBPHFaceRecognizer_create()
        self.recognizer.read('detection/utils/trainer/trainer.yml')
        cascadePath = directory_path + "haarcascade_frontalface_default.xml"
        self.faceCascade = cv2.CascadeClassifier(cascadePath)

    def detect(self, image):
        
        if image is None:
            raise Exception("Detector-detect error: image is None")

        image_np = np.array(cv2.imdecode(
            np.fromstring(image, np.uint8), cv2.IMREAD_COLOR))
        gray = cv2.cvtColor(image_np, cv2.COLOR_BGR2GRAY)
        gray = cv2.resize(gray, (int(minW) * 10, int(minH) *10))

        faces = self.faceCascade.detectMultiScale( 
            gray, 1.18, 8
        )

        for(x,y,w,h) in faces:

            cv2.rectangle(image_np, (x,y), (x+w,y+h), (0,255,0), 2)
            print("asd")
            id, confidence = self.recognizer.predict(gray[y:y+h,x:x+w])
            print(id, confidence)
            # Check if confidence is less them 100 ==> "0" is perfect match 
            if (confidence < 100):
                persons.append(names[id])
                probabilites.append("  {0}%".format(round(confidence)))
            else:
                persons.append("unknown")
                probabilites.append("  {0}%".format(round(confidence)))

        return persons, probabilites