from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from detection import EmotionDetector
from detection import FaceDetector
from detection import SpeechDetector
from detection import MovementDetector
import cv2

IMAGE_FIELD = 'image'
SOUND_FIELD = 'speech'
SUPPORTED_IMAGE_EXTENSIONS = ('.png', '.jpg', '.jpeg')
SUPPORTED_SOUND_EXTENSIONS = ('.mp3', '.wav')
JSON_ERROR_MESSAGE_FIELD = 'message'

class EmotionDetectionEndpoint(APIView):

    detector = EmotionDetector()

    def __init__(self, name=None):
        self.name = name

    def post(self, request):

        if IMAGE_FIELD not in request.FILES:
            return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Could not find image" },
                            status=status.HTTP_404_NOT_FOUND)

        image = None

        if request.FILES[IMAGE_FIELD].name.lower().endswith(SUPPORTED_IMAGE_EXTENSIONS):
            try:
                image = request.FILES[IMAGE_FIELD].read()
            except Exception as exception:
                print("DetectionApi POST method error:", exception)
                return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Something went wrong" },
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({JSON_ERROR_MESSAGE_FIELD: "Unsupported file type"},
                                status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
        probabilities, states = EmotionDetectionEndpoint.detector.detect(image=image)
        print(probabilities, states)
        return JsonResponse({ 'probabilities': probabilities,'states': states })

class FaceDetectionEndpoint(APIView):

    detector = FaceDetector()

    def __init__(self, name=None):
        self.name = name

    def post(self, request):

        if IMAGE_FIELD not in request.FILES:
            return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Could not find image" },
                            status=status.HTTP_404_NOT_FOUND)

        image = None

        if request.FILES[IMAGE_FIELD].name.lower().endswith(SUPPORTED_IMAGE_EXTENSIONS):
            try:
                image = request.FILES[IMAGE_FIELD].read()
            except Exception as exception:
                print("DetectionApi POST method error:", exception)
                return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Something went wrong" },
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({JSON_ERROR_MESSAGE_FIELD: "Unsupported file type"},
                                status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
        persons, probabilities = FaceDetectionEndpoint.detector.detect(image=image)
        print(persons, probabilities)
        return JsonResponse({ 'persons': persons,'probabilities': probabilities })

class SpeechDetectionEndpoint(APIView):

    detector = SpeechDetector()
    
    def __init__(self, name=None):
        self.name = name

    def post(self, request):

        if SOUND_FIELD not in request.FILES:
            return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Could not find sound file" },
                            status=status.HTTP_404_NOT_FOUND)

        sound = None

        if request.FILES[SOUND_FIELD].name.lower().endswith(SUPPORTED_SOUND_EXTENSIONS):
            try:
                sound = request.FILES[SOUND_FIELD].read()
            except Exception as exception:
                print("DetectionApi POST method error:", exception)
                return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Something went wrong" },
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({JSON_ERROR_MESSAGE_FIELD: "Unsupported file type"},
                                status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
        result = SpeechDetectionEndpoint.detector.detect(speech=sound)
        print(result)
        return JsonResponse({ 'state': result })

class MovementDetectionEndpoint(APIView):

    detector = MovementDetector()

    def __init__(self, name=None):
        self.name = name

    def post(self, request):

        if IMAGE_FIELD not in request.FILES:
            return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Could not find image" },
                            status=status.HTTP_404_NOT_FOUND)

        image = None

        if request.FILES[IMAGE_FIELD].name.lower().endswith(SUPPORTED_IMAGE_EXTENSIONS):
            try:
                image = request.FILES[IMAGE_FIELD].read()
            except Exception as exception:
                print("DetectionApi POST method error:", exception)
                return JsonResponse({ JSON_ERROR_MESSAGE_FIELD: "Something went wrong" },
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({JSON_ERROR_MESSAGE_FIELD: "Unsupported file type"},
                                status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
        persons, probabilities = FaceDetectionEndpoint.detector.detect(image=image)
        print(persons, probabilities)
        return JsonResponse({ 'persons': persons,'probabilities': probabilities })