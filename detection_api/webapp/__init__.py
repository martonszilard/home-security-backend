from webapp.detection_endpoint import EmotionDetectionEndpoint
from webapp.detection_endpoint import FaceDetectionEndpoint
from webapp.detection_endpoint import SpeechDetectionEndpoint
from webapp.detection_endpoint import MovementDetectionEndpoint